import VueRouter from "vue-router";

import Home from "@/views/Home.vue";
import MealDetails from "@/views/MealDetails.vue";

const routes = [
  { path: "/", name: "Home", component: Home },
  { path: "/meal/:id", name: "MealDetail", component: MealDetails },
];

export default new VueRouter({
  mode: "history",
  routes,
});
