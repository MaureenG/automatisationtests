// https://docs.cypress.io/api/introduction/api.html
describe("Test", () => {
  before(() => {
    cy.server();
    cy.route("**/random.php", "fixture:meal.json").as("getMeal");
    cy.visit("/");
  });

  it("Verification of items existance", () => {
    cy.wait("@getMeal").then(() => {
      cy.get(".cards").should("not.be.empty");
    });
  });

  it("Checking NavBar existance", () => {
    cy.get("nav").should("not.be.empty");
  });
});
