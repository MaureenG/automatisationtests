import { shallowMount } from "@vue/test-utils";
import ListMeal from "@/components/ListMeal";
const meal = require("./fixtures/meal.json");

//Unitaire
describe("ListMeal.vue", () => {
  it("Vérifie le nombre de menu présent", () => {
    const wrapper = shallowMount(ListMeal, {
      propsData: { meals: [meal, meal] },
    });
    expect(wrapper.find(".cards").findAll(".item")).toHaveLength(2);
  });
  it("Test de la méthode getCountryCodeFromArea", () => {
    const wrapper = shallowMount(ListMeal, {
      propsData: { meals: [meal] },
    });
    expect(wrapper.vm.getCountryCodeFromArea("American")).toEqual("us");
  });
});
